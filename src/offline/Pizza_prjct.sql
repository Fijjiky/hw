/*CREATE TABLE PIZZA(
pizza_id INT NOT NULL PRIMARY KEY,
pizza_name VARCHAR(60),
pizza_comment VARCHAR(120));

CREATE TABLE COMPONENTS(
component_id INT NOT NULL PRIMARY KEY,
component_name VARCHAR(60),
component_price NUMBER(2));

CREATE TABLE PIZZA_COMPONENTS(
pc_pizza_id INT,
pc_component_id INT,
pc_amount NUMBER(2),
FOREIGN KEY (pc_pizza_id) REFERENCES PIZZA (pizza_id),
FOREIGN KEY(pc_component_id) REFERENCES COMPONENTS (component_id));

CREATE TABLE PIZZA_ORDERS(
po_pizza_id INT NOT NULL,
po_order_id INT NOT NULL,
po_amount NUMBER(2),
FOREIGN KEY (po_pizza_id) REFERENCES PIZZA (pizza_id),
FOREIGN KEY(po_order_id) REFERENCES ORDERS (order_id));

CREATE TABLE ORDERS(order_id INT NOT NULL PRIMARY KEY);


INSERT INTO ORDERS (ORDER_ID)
VALUES ('1');
INSERT INTO ORDERS (ORDER_ID)
VALUES ('2');

INSERT INTO PIZZA (po_pizza_id, pizza_name, pizza_comment)
VALUES ('2','BBQ', 'Spicy');
INSERT INTO PIZZA (pizza_id, pizza_name, pizza_comment)
VALUES ('1','Margarita', 'Nice taste');
INSERT INTO PIZZA (pizza_id, pizza_name, pizza_comment)
VALUES ('3','Butterfly', 'A lot of Butter');

INSERT INTO COMPONENTS (component_id, component_name, COMPONENT_PRICE)
VALUES ('1','Cheeze', '13');
INSERT INTO COMPONENTS (component_id, component_name, COMPONENT_PRICE)
VALUES ('2','Pepper', '20');
INSERT INTO COMPONENTS (component_id, component_name, COMPONENT_PRICE)
VALUES ('3','Mustard', '8');
INSERT INTO COMPONENTS (component_id, component_name, COMPONENT_PRICE)
VALUES ('4','Butter', '5');

INSERT INTO PIZZA_ORDERS (po_pizza_id, po_order_id, po_amount)
VALUES ('1','1', '1');
INSERT INTO PIZZA_ORDERS (po_pizza_id, po_order_id, po_amount)
VALUES ('2','2', '3');

INSERT INTO PIZZA_COMPONENTS (pc_pizza_id, pc_component_id, pc_amount)
VALUES ('1','1', '10');
INSERT INTO PIZZA_COMPONENTS (pc_pizza_id, pc_component_id, pc_amount)
VALUES ('2','2', '2');
INSERT INTO PIZZA_COMPONENTS (pc_pizza_id, pc_component_id, pc_amount)
VALUES ('1','3', '3');
INSERT INTO PIZZA_COMPONENTS (pc_pizza_id, pc_component_id, pc_amount)
VALUES ('3','4', '8');
*/

SELECT pizza_name,  /*ex.4*/
       component_name,
       pc_pizza_id,
       pc_component_id
FROM Pizza INNER JOIN pizza_components ON pizza_id = pc_pizza_id
                INNER JOIN COMPONENTS ON component_id = pizza_components.pc_component_id
                where pizza_name = 'Margarita';

SELECT MAX(COMPONENT_PRICE) FROM COMPONENTS;
SELECT * FROM COMPONENTS
where COMPONENT_PRICE =(SELECT MIN(COMPONENT_PRICE) FROM COMPONENTS); /*ex.5*/

SELECT SUM(COMPONENT_PRICE) /*ex.7*/
from components INNER JOIN PIZZA_COMPONENTS ON components.COMPONENT_ID = PIZZA_COMPONENTS.PC_COMPONENT_ID
                INNER JOIN PIZZA ON pizza_components.PC_PIZZA_ID = PIZZA.PIZZA_ID
                where pizza_name = 'Margarita';

SELECT DISTINCT pizza_name,  /*ex.8*/
       pc_pizza_id
FROM Pizza INNER JOIN pizza_components ON pizza_id = pc_pizza_id
                INNER JOIN COMPONENTS ON component_id = pizza_components.pc_component_id
                where COMPONENTS.COMPONENT_NAME != 'Butter';

SELECT pizza_name,  /*ex.9*/
COMPONENTS.COMPONENT_PRICE
FROM Pizza INNER JOIN pizza_components ON pizza_id = pc_pizza_id
                INNER JOIN COMPONENTS ON component_id = pizza_components.pc_component_id
                where  component_price = (SELECT MAX(COMPONENT_PRICE) FROM COMPONENTS);

