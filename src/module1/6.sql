SELECT AVG(dev_salary) AS JavaProgAVR FROM (
SELECT dev_salary,
       projects.pj_name,
       projects.pj_cost
FROM developers INNER JOIN dev_pj ON developers.dev_id = dev_pj.dev_id
                INNER JOIN projects ON projects.pj_ID = dev_pj.pj_ID
                Where ( pj_cost =(SELECT MIN(pj_cost) FROM projects)));