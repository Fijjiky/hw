SELECT pj_name,
       projects.pj_cost,
       dev_salary
FROM developers INNER JOIN dev_pj ON developers.dev_id = dev_pj.dev_id
                INNER JOIN projects ON projects.pj_ID = dev_pj.pj_ID
                Where ( dev_salary =(SELECT MAX(dev_salary) FROM developers));