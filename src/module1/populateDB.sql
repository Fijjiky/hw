/* USE 1.sql to add dev_salary before inserting */
/* USE 4.sql to add pj_cost before inserting */

INSERT INTO developers (dev_id, dev_name, dev_salary)
VALUES ('1', 'DANIL', '2000');
INSERT INTO developers (dev_id, dev_name,  dev_salary)
VALUES ('2', 'DEN', '3000');
INSERT INTO developers (dev_id, dev_name,  dev_salary)
VALUES ('3', 'DAVID', '8000');
INSERT INTO developers (dev_id, dev_name,  dev_salary)
VALUES ('4', 'Smith', '5000');
INSERT INTO developers (dev_id, dev_name,  dev_salary)
VALUES ('5', 'Jane', '1500');
INSERT INTO developers (dev_id, dev_name,  dev_salary)
VALUES ('6', 'Jimmy', '2200');
INSERT INTO developers (dev_id, dev_name, dev_salary)
VALUES ('7', 'Ben', '1900');
INSERT INTO developers (dev_id, dev_name,  dev_salary)
VALUES ('8', 'Rustam', '12000');

INSERT INTO skills (sk_id, sk_name)
VALUES ('1', 'Java');
INSERT INTO skills (sk_id, sk_name)
VALUES ('2', 'JavaScript');
INSERT INTO skills (sk_id, sk_name)
VALUES ('3', 'Ruby');
INSERT INTO skills (sk_id, sk_name)
VALUES ('4', 'C#');
INSERT INTO skills (sk_id, sk_name)
VALUES ('5', 'C++');
INSERT INTO skills (sk_id, sk_name)
VALUES ('6', 'PHP');
INSERT INTO skills (sk_id, sk_name)
VALUES ('7', 'Python');

INSERT INTO projects (pj_id, pj_name, pj_cost)
VALUES ('1', 'BANK soft', '60000');
INSERT INTO projects (pj_id, pj_name, pj_cost)
VALUES ('2', 'Hotel soft', '80000');
INSERT INTO projects (pj_id, pj_name, pj_cost)
VALUES ('3', 'ERP prjct', '120000');
INSERT INTO projects (pj_id, pj_name, pj_cost, pj_desc)
VALUES ('4', 'Android N update', '24000', 'Upgrade camera software');

INSERT INTO company (com_id, com_name)
VALUES ('1', 'Google');
INSERT INTO company (com_id, com_name)
VALUES ('2', 'Apple');
INSERT INTO company (com_id, com_name)
VALUES ('3', 'Samsung');
INSERT INTO company (com_id, com_name)
VALUES ('4', 'Bolliwood');
INSERT INTO company (com_id, com_name)
VALUES ('5', 'IBM');

INSERT INTO customers (customer_id, customer_name)
VALUES ('1', 'MiMiSoft');
INSERT INTO customers (customer_id, customer_name)
VALUES ('2', 'ZuRu');
INSERT INTO customers (customer_id, customer_name)
VALUES ('3', 'Zelman');
INSERT INTO customers (customer_id, customer_name)
VALUES ('4', 'Cona');

INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('1', '1');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('2', '1');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('3', '2');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('4', '2');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('5', '3');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('6', '3');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('1', '4');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('7', '5');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('3', '6');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('3', '7');
INSERT INTO DEV_SKILLS (sk_id, dev_id)
VALUES ('3', '8');

INSERT INTO dev_pj (pj_id, dev_id)
VALUES ('1', '1');
INSERT INTO dev_pj (pj_id, dev_id)
VALUES ('1', '2');
INSERT INTO dev_pj (pj_id, dev_id)
VALUES ('2', '3');
INSERT INTO dev_pj (pj_id, dev_id)
VALUES ('2', '4');
INSERT INTO dev_pj (pj_id, dev_id)
VALUES ('3', '5');
INSERT INTO dev_pj (pj_id, dev_id)
VALUES ('3', '6');
INSERT INTO dev_pj (pj_id, dev_id)
VALUES ('4', '7');
INSERT INTO dev_pj (pj_id, dev_id)
VALUES ('4', '8');

INSERT INTO com_dev (com_id, dev_id)
VALUES ('1', '1');
INSERT INTO com_dev (com_id, dev_id)
VALUES ('2', '2');
INSERT INTO com_dev (com_id, dev_id)
VALUES ('3', '3');
INSERT INTO com_dev (com_id, dev_id)
VALUES ('4', '4');
INSERT INTO com_dev (com_id, dev_id)
VALUES ('5', '5');
INSERT INTO com_dev (com_id, dev_id)
VALUES ('2', '6');
INSERT INTO com_dev (com_id, dev_id)
VALUES ('4', '7');
INSERT INTO com_dev (com_id, dev_id)
VALUES ('5', '8');

INSERT INTO com_pj (pj_id, com_id)
VALUES ('1', '1');
INSERT INTO com_pj (pj_id, com_id)
VALUES ('2', '2');
INSERT INTO com_pj (pj_id, com_id)
VALUES ('3', '3');
INSERT INTO com_pj (pj_id, com_id)
VALUES ('4', '4');
INSERT INTO com_pj (pj_id, com_id)
VALUES ('1', '5');
INSERT INTO com_pj (pj_id, com_id)
VALUES ('2', '4');
INSERT INTO com_pj (pj_id, com_id)
VALUES ('4', '3');

INSERT INTO customers_pj (pj_id, customer_id)
VALUES ('1', '1');
INSERT INTO customers_pj (pj_id, customer_id)
VALUES ('2', '2');
INSERT INTO customers_pj (pj_id, customer_id)
VALUES ('3', '3');
INSERT INTO customers_pj (pj_id, customer_id)
VALUES ('4', '4');
INSERT INTO customers_pj (pj_id, customer_id)
VALUES ('2', '1');
INSERT INTO customers_pj (pj_id, customer_id)
VALUES ('3', '2');