SELECT SUM(dev_salary) AS JavaProgSalary FROM (
SELECT developers.dev_name,
       skills.sk_name,
       developers.dev_id,
       dev_salary
FROM developers INNER JOIN dev_skills ON developers.dev_id = dev_skills.dev_id
                INNER JOIN skills ON skills.SK_ID = dev_skills.SK_ID
                where skills.sk_name = 'Java');