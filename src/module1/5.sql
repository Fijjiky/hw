SELECT company.com_name ,
       customers.customer_name,
       pj_name,
       projects.pj_cost
       FROM company JOIN com_pj ON com_pj.COM_ID = company.COM_ID
             JOIN projects ON projects.pj_id = com_pj.PJ_ID
             left JOIN customers_pj ON customers_pj.pj_id = projects.PJ_ID
             left JOIN customers ON customers.customer_id = customers_pj.CUSTOMER_ID
             Order by pj_cost asc;